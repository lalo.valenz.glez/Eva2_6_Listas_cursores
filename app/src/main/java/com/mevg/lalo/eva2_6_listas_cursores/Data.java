package com.mevg.lalo.eva2_6_listas_cursores;

/**
 * Created by yjm_e on 13/11/2016.
 */

public class Data {
    String name;
    String phone;

    public Data(String name, String phone){
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
