package com.mevg.lalo.eva2_6_listas_cursores;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.TextView;

import java.util.List;


/**
 * Created by yjm_e on 13/11/2016.
 */

public class DataAdapter extends ArrayAdapter<Data> {

    LayoutInflater inflater;

    public DataAdapter(Context context, int resource, List<Data> objects) {
        super(context, resource, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = inflater.inflate(R.layout.data_list, null);
        }
        Data d =  getItem(position);

        if(d != null){

            Log.i("nulo", "" + (R.id.tvname));

            TextView tvName = (TextView) view.findViewById(R.id.tvname);

            TextView tvPhone = (TextView) view.findViewById(R.id.tvphone);

            if(tvName != null)
                tvName.setText(d.getName());

            if(tvPhone != null)
                tvPhone.setText(d.getPhone());

        }
        return view;
    }
}
