package com.mevg.lalo.eva2_6_listas_cursores;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView lvDataView;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvDataView = (ListView) findViewById(R.id.dataView);
        db = openOrCreateDatabase("app", MODE_PRIVATE, null);
        db.execSQL("drop table if exists data");
        db.execSQL("create table if not exists data(" +
                   "id integer primary key autoincrement, " +
                   "name text, " +
                   "phone text)");
        db.execSQL("insert into data (name, phone) values ('lalo', '1111111')");
        db.execSQL("insert into data (name, phone) values ('ana', '2222222')");
        db.execSQL("insert into data (name, phone) values ('alan', '3333333')");
        db.execSQL("insert into data (name, phone) values ('majo', '44444444')");
        db.execSQL("insert into data (name, phone) values ('yo', '55555555')");
        db.execSQL("insert into data (name, phone) values ('tu', '66666666')");
        db.execSQL("insert into data (name, phone) values ('alguien', '1111111')");
        db.execSQL("insert into data (name, phone) values ('todos', '2222222')");
        db.execSQL("insert into data (name, phone) values ('aquellos', '3333333')");
        db.execSQL("insert into data (name, phone) values ('prros', '44444444')");
        db.execSQL("insert into data (name, phone) values ('tu papa', '55555555')");
        db.execSQL("insert into data (name, phone) values ('el papa de edgar', '66666666')");
        db.execSQL("insert into data (name, phone) values ('el papa de alan', '1111111')");
        db.execSQL("insert into data (name, phone) values ('ana lo hizo', '2222222')");
        db.execSQL("insert into data (name, phone) values ('pato', '3333333')");
        db.execSQL("insert into data (name, phone) values ('otro pato', '44444444')");
        db.execSQL("insert into data (name, phone) values ('pokemon', '55555555')");
        db.execSQL("insert into data (name, phone) values ('tu papa otra vez', '66666666')");

        Cursor query = db.rawQuery("SELECT * FROM data",null, null);
        final List<Data> list = new ArrayList<>();
        query.moveToFirst();
        while(!query.isAfterLast()){
            Data data = new Data(query.getString(1),query.getString(2));
            list.add(data);
            query.moveToNext();
        }

        DataAdapter adapter = new DataAdapter(this, R.layout.data_list, list);

        lvDataView.setAdapter(adapter);

        lvDataView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Data d = list.get(position);
                String name = d.getName();
                String phone = d.getPhone();
                Toast.makeText(MainActivity.this, "Nombre: " + name + "\nPhone: " +phone , Toast.LENGTH_SHORT).show();
            }
        });
    }
}
